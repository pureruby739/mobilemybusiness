package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.entity.Point
import com.google.gson.GsonBuilder
import okhttp3.*
import java.io.IOException

class PointActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_point)
        val list: List<Point> = intent.getSerializableExtra("points") as List<Point>
        println(list)
        val recycler_view_points = findViewById<RecyclerView>(R.id.recycler_view_points)
        recycler_view_points.layoutManager = LinearLayoutManager(this)
        recycler_view_points.adapter = PointAdapter(list)
        findViewById<ImageButton>(R.id.logout)
            .setOnClickListener(){
                logOut()
            }
    }
    fun logOut(){
        val url1 = "http://192.168.0.101:8000/api/auth/logout/"
        val request = Request.Builder().addHeader(
            "Authorization",
            "token ${MainActivity.token}"
        ).url(url1).build()

        val client = OkHttpClient()

        client.newCall(request).enqueue(
            object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    println(e)
                }

                override fun onResponse(call: Call, response: Response) {
                    startActivity(Intent(this@PointActivity, AuthActivity::class.java))
                }
            }
        )

    }
}
