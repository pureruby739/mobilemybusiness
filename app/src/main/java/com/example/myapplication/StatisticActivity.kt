package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myapplication.R
import com.example.myapplication.entity.Point
import com.example.myapplication.entity.Value
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry

class StatisticActivity : AppCompatActivity() {
    private lateinit var list: List<Value>

    companion object{
        lateinit var list: List<Value>
    }
    fun setList(listt: List<Value>){
        StatisticActivity.list = listt
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistic)
        val list: List<Value> = intent.getSerializableExtra("values") as List<Value>
        println(list)
        this.list = list
        setList(list)
        setBarChart()
    }
    private fun setBarChart() {
        val list = this@StatisticActivity.list
        var entries:ArrayList<BarEntry> = arrayListOf()
        val labels: MutableList<String?> = mutableListOf()

        list.forEachIndexed{
                index, element ->
                entries.add(BarEntry(element.value.toFloat(), index))
                labels.add(element.time)
        }

        val barDataSet = BarDataSet(entries, "people")

        val data = BarData(labels, barDataSet)
        val barChart = findViewById<com.github.mikephil.charting.charts.BarChart>(R.id.barChart)
        barChart.data = data

        barDataSet.setColor(R.color.orange)
            barChart.animateY(5000)
    }
}