package com.example.myapplication.entity

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable
import java.util.ArrayList

data class Point(
    val pointId: Long,
    val location: String?,
    val rating: Long,
    val maxVal: Long,
    val organisationId: Long,
    val values: ArrayList<Value>?
) : Serializable, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readLong(),
        parcel.readLong(),
        parcel.createTypedArrayList(Value)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(pointId)
        parcel.writeString(location)
        parcel.writeLong(rating)
        parcel.writeLong(maxVal)
        parcel.writeLong(organisationId)
        parcel.writeTypedList(values)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Point> {
        override fun createFromParcel(parcel: Parcel): Point {
            return Point(parcel)
        }

        override fun newArray(size: Int): Array<Point?> {
            return arrayOfNulls(size)
        }
    }

}