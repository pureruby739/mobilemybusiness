package com.example.myapplication.entity

data class UserApp(
    val token: String,
    val id: Int,
)
