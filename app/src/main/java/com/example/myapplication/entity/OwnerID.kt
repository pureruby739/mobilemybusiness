package com.example.myapplication.entity

data class OwnerID (
    val email: String,
    val username: String
)