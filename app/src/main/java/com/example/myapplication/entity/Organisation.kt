package com.example.myapplication.entity

data class Organisation (
    val organisationID: Long,
    val name: String,
    val ownerID: OwnerID,
    val points: List<Point>
)