package com.example.myapplication.entity

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

data class Value(
    val valueID: Long,
    val pointID: Long,
    val day: String?,
    val time: String?,
    val value: Long
) : Serializable, Parcelable{
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readLong()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(valueID)
        parcel.writeLong(pointID)
        parcel.writeString(day)
        parcel.writeString(time)
        parcel.writeLong(value)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Value> {
        override fun createFromParcel(parcel: Parcel): Value {
            return Value(parcel)
        }

        override fun newArray(size: Int): Array<Value?> {
            return arrayOfNulls(size)
        }
    }
}