package com.example.myapplication

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.entity.Point
import com.example.myapplication.entity.Value
import java.io.Serializable
import java.util.ArrayList

class PointAdapter (val points: List<Point>) : RecyclerView.Adapter<CustomViewHolder>() {
    val pointListName = points

    override fun getItemCount(): Int {
        return  points.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(
            R.layout.item_point,
            parent,
            false
        )
        return CustomViewHolder(v)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val pointName = holder.view.findViewById<TextView>(R.id.pointNameTv)
        val organisationIcon = holder.view.findViewById<ImageView>(R.id.pointIcon)
        val pointListName = pointListName.get(position).location

        organisationIcon.setImageResource(R.drawable.ic_baseline_location_on_24)
        pointName.setText(pointListName)

        holder.itemView.setOnClickListener{
            val intent = Intent(holder.view.context, StatisticActivity::class.java)
            val values: ArrayList<Value>? = this.pointListName.get(position).values
            holder.view.context.startActivity(intent)
            intent.putExtra("values", values as Serializable)
            holder.view.context.startActivity(intent)
        }
    }
}
