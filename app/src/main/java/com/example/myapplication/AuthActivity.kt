package com.example.myapplication
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.widget.Button
import android.widget.EditText
import com.example.myapplication.entity.UserApp
import com.google.gson.GsonBuilder
import okhttp3.*
import java.io.IOException

class AuthActivity : AppCompatActivity() {
    private lateinit var token: String
    private lateinit var username: String
    private lateinit var password: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_auth)
        findViewById<Button>(R.id.login_button_login)
            .setOnClickListener {
            val username = findViewById<EditText>(R.id.username_edittext_login).text.toString()
            val password = findViewById<EditText>(R.id.password_edittext_login).text.toString()
                setUserData(username, password)
                getAuthUser()
            }
    }

    private fun setUserData(username: String, password: String){
        this.username = username
        this.password = password
    }

    fun setToken(token: String){
        this.token = token
        val intent: Intent = Intent(this, MainActivity::class.java)
            .apply {
                putExtra(EXTRA_MESSAGE, token)
            }
        startActivity(intent)
    }

    private fun getAuthUser(){
        val formBody = FormBody.Builder()
            .add("username", "$username").add("password","$password")
            .build()

        val url1 = "http://192.168.0.101:8000/api/authenticate/"
        val request = Request.Builder()
            .url(url1)
            .post(formBody)
            .build()
        val client = OkHttpClient()

        client.newCall(request).enqueue(
            object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    println(e)
                }

                override fun onResponse(call: Call, response: Response) {
                    val body = response.body()?.string()
                    val gson = GsonBuilder().create()

                    val organisation = gson.fromJson(body, UserApp::class.java)
                    setToken(organisation.token)
                }
            }
        )
    }
}