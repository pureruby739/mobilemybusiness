package com.example.myapplication

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.entity.Organisation
import com.example.myapplication.entity.Point
import java.io.Serializable

class MainAdapter(private val organisations: List<Organisation>) :
    RecyclerView.Adapter<CustomViewHolder>() {
    private val organisationListName = organisations

    override fun getItemCount(): Int {
        return  organisations.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_organisation,
            parent,
            false
        )
        return CustomViewHolder(view)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        val organisationName = holder.view.findViewById<TextView>(R.id.organisationNameTv)
        val organisationIcon = holder.view.findViewById<ImageView>(R.id.organisationIcon)
        organisationIcon.setImageResource(R.drawable.ic_baseline_location_city_24)
        val organisationListName = organisationListName.get(position).name
        organisationName.setText(organisationListName)
        holder.itemView.setOnClickListener{
            val intent = Intent(holder.view.context, PointActivity::class.java)
            val point: List<Point> = this.organisationListName.get(position).points
            intent.putExtra("points", point as Serializable)
            holder.view.context.startActivity(intent)
        }
    }
}

class CustomViewHolder(val view: View): RecyclerView.ViewHolder(view)

