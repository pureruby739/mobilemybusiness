package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.widget.ImageButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.entity.Organisation
import com.google.gson.GsonBuilder
import okhttp3.*
import java.io.IOException

class MainActivity : AppCompatActivity() {

    private lateinit var token: String

    companion object{
        lateinit var token: String
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val message = intent.getStringExtra(EXTRA_MESSAGE)
        this.token = message.toString()
        MainActivity.token = token
        setContentView(R.layout.activity_main)
        val pointsRecyclerView = findViewById<RecyclerView>(R.id.recycler_view_points)
        pointsRecyclerView.layoutManager = LinearLayoutManager(this)
        fetchJson()
        findViewById<ImageButton>(R.id.logout)
            .setOnClickListener(){
                logOut()
            }
    }


    fun logOut() {
            val url1 = "http://192.168.0.101:8000/api/auth/logout/"
            val request = Request.Builder().addHeader("Authorization", "token $token")
                .url(url1).build()

            val client = OkHttpClient()

            client.newCall(request).enqueue(
                object: Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        println(e)
                    }

                    override fun onResponse(call: Call, response: Response) {
                        startActivity(
                            Intent(this@MainActivity, AuthActivity::class.java)
                        )
                    }
                }
            )
    }

    fun fetchJson() {
        val url1 = "http://192.168.0.101:8000/api/organisations/all"
        val request = Request.Builder().addHeader("Authorization", "token $token")
            .url(url1).build()

        val client = OkHttpClient()

        client.newCall(request).enqueue(
            object: Callback{
                override fun onFailure(call: Call, e: IOException) {
                    println(e)
                }

                override fun onResponse(call: Call, response: Response) {
                    val body = response.body()?.string()
                    val gson = GsonBuilder().create()

                    val organisation = gson.fromJson(body, Array<Organisation>::class.java).toList()

                    runOnUiThread {
                        findViewById<RecyclerView>(R.id.recycler_view_points)
                            .adapter = MainAdapter(organisation)
                    }
                }
            }
        )
    }
}
